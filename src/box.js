/**
 * @module Box helpers
 * Having top, left, bottom, right seperated makes it easier to override and maintain individual properties
 */
const boxUnitToString = value => typeof value === 'number' ? value.toString() + 'px' : value


/**
 * Takes a function that expects Box to be passed into it
 * and creates a BoxFunction
 */
const createBoxFunction = (mapFromBox) => {

  const result= (a, b, c, d) => {

    if (b === undefined && c === undefined && d === undefined) {
      b = c = d = a
    }
    else if (c === undefined && d === undefined) {
      c = a
      d = b
    }

    let box = {
      top: boxUnitToString(a),
      right: boxUnitToString(b),
      bottom: boxUnitToString(c),
      left: boxUnitToString(d)
    }

    return mapFromBox(box)
  }
  return result
}

export const padding = createBoxFunction(box => ({
  paddingTop: box.top,
  paddingRight: box.right,
  paddingBottom: box.bottom,
  paddingLeft: box.left
}))

export const margin = createBoxFunction(box => ({
  marginTop: box.top,
  marginRight: box.right,
  marginBottom: box.bottom,
  marginLeft: box.left
}))

export const border = createBoxFunction(box => ({
  borderTop: box.top,
  borderRight: box.right,
  borderBottom: box.bottom,
  borderLeft: box.left
}))

/** Puts a vertical margin between each child */
export const verticallySpaced = margin => ({
  '&>*': {
    marginBottom: boxUnitToString(margin) + ' !important'
  },
  '&>*:last-child': {
    marginBottom: '0px !important',
  }
})

/** Puts a horizontal margin between each child */
export const horizontallySpaced = margin => ({
  '&>*': {
    marginRight: boxUnitToString(margin) + ' !important'
  },
  '&>*:last-child': {
    marginRight: '0px !important',
  }
})

/** Puts a (horizontal AND vertical) margin between each child */
export function gridSpaced(topAndBottom, leftAndRight = topAndBottom) {

  if (leftAndRight === void 0)  
    leftAndRight = topAndBottom 

  const vertical = boxUnitToString(topAndBottom)
  const horizontal = boxUnitToString(leftAndRight)

  return ({
    marginTop: '-' + vertical,
    marginLeft: '-' + horizontal,
    '&>*': {
      marginTop: vertical,
      marginLeft: horizontal,
    }
  })
}

/** Gives this element the same size as the nearest offsetParent */
export const fillParent = {
  width: '100%',
  height: '100%',
}

/** mixin: maxWidth */
export const maxWidth = value => ({
  maxWidth: boxUnitToString(value)
})

/** mixin: maxHeight */
export const maxHeight = value => ({
  maxHeight: boxUnitToString(value)
})

/** Block elements: Centering *self* using margins */
export const horizontallyCenterSelf = {
  marginLeft: 'auto',
  marginRight: 'auto',
}

/** Block elements: Centering *child* elements using textAlign */
export const horizontallyCenterChildren = {
  textAlign: 'center'
}

/** mixin: height */
export const height = value => ({
  height: boxUnitToString(value)
})

/** mixin: width */
export const width = value => ({
  width: boxUnitToString(value)
})
