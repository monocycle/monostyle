import { px } from './units.js'





const delimited = function (delimiter) {
  return function (...args) {
    return args
      .filter(s => s || s === 0)
      .map(function (s) { return typeof s === 'number' ? px(s) : s.toString() })
      .join(delimiter)
  }
}

export const params = delimited(' ')

export const list = delimited(',')
