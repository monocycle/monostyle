/**
 * New layer parent
 */
export const layerParent = {
  position: 'relative'
}

/**
 * Use this to attach to any parent layer
 * and then you can use `left`/`top` etc to position yourself
 */
export const attachToLayerParent = {
  position: 'absolute'
}

export const top = { top: '0' }
export const bottom = { bottom: '0' }
export const left = { left: '0' }
export const right = { right: '0' }


/**
 * This new layer will attach itself to the nearest parent with `position:relative` or `position:absolute`
 * And will become the new `layerParent`
 */
export const newLayer = Object.assign(
  {},
  attachToLayerParent,
  left,
  right,
  top,
  bottom
)

export const attachToTop = Object.assign(
  {},
  attachToLayerParent,
  top,
  left,
  right
)

export const attachToRight = Object.assign(
  {},
  attachToLayerParent,
  top,
  right,
  bottom
)

export const attachToBottom = Object.assign(
  {},
  attachToLayerParent,
  right,
  bottom,
  left
)

export const attachToLeft = Object.assign(
  {},
  attachToLayerParent,
  top,
  bottom,
  left
)

/**
 * Helps fixing to page
 */
export const fixed = {
  position: 'fixed'
}

export const pageTop = Object.assign(
  {},
  fixed,
  top,
  left,
  right
)

export const pageRight = Object.assign(
  {},
  fixed,
  top,
  right,
  bottom
)

export const pageBottom = Object.assign(
  {},
  fixed,
  right,
  bottom,
  left
)

export const pageLeft = Object.assign(
  {},
  fixed,
  top,
  bottom,
  left
)