import { formatUnit } from './utilities/formatting.js'





export const percent = formatUnit('%')
export const deg = formatUnit('deg')
export const em = formatUnit('em')
export const ex = formatUnit('ex')
export const px = formatUnit('px')
export const rad = formatUnit('rad')
export const rem = formatUnit('rem')
export const viewHeight = formatUnit('vh')
export const viewWidth = formatUnit('vw')
export const turn = formatUnit('turn')
