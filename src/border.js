import { ensureLength } from './utilities/formatting.js'
import { params } from './lists.js'





/**
 * Returns the value with '' around it.  Any 's will be escaped \' in the output
 */
export function border(p) {

  return params(
    p.color,
    p.style,
    ensureLength(p.width)
  )
}

export const borderColor = params
export const borderStyle = params
export const borderWidth = params
