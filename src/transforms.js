import { createFunction } from './utilities/formatting.js'





/**
 * The CSS transform property lets you modify the coordinate space of the CSS visual formatting model. Using it, elements can be translated, rotated, scaled, and skewed.
 * Returns the transforms as a delimited string by space or returns 'none' if no arguments are provided
 * @see https://developer.mozilla.org/en-US/docs/Web/CSS/transform
 */
export function transform() {

  const transforms = []

  for (let _i = 0; _i < arguments.length; _i++)
    transforms[_i] = arguments[_i]

  return transforms.length
    ? transforms.join(' ')
    : 'none'
}

export const matrix = createFunction('matrix')
export const matrix3d = createFunction('matrix3d')
export const perspective = createFunction('perspective')
export const rotate = createFunction('rotate')
export const rotate3d = createFunction('rotate3d')
export const rotateX = createFunction('rotateX')
export const rotateY = createFunction('rotateY')
export const rotateZ = createFunction('rotateZ')
export const scale = createFunction('scale')
export const scale3d = createFunction('scale3d')
export const scaleX = createFunction('scaleX')
export const scaleY = createFunction('scaleY')
export const scaleZ = createFunction('scaleZ')
export const skew = createFunction('skew')
export const skewX = createFunction('skewX')
export const skewY = createFunction('skewY')
export const translate = createFunction('translate')
export const translate3d = createFunction('translate3d')
export const translateX = createFunction('translateX')
export const translateY = createFunction('translateY')
export const translateZ = createFunction('translateZ')
