/**
 * @module scroll helpers
 */
export const scroll = {
  '-webkit-overflow-scrolling': 'touch',
  overflow: 'auto'
}

export const scrollX = {
  '-webkit-overflow-scrolling': 'touch',
  overflowX: 'auto'
}

export const scrollY = {
  '-webkit-overflow-scrolling': 'touch',
  overflowY: 'auto'
}

/**
 * If you expect a child somewhere down in the tree to scroll
 * you need to tell the browser to prevent a scroll bar. 
 * Use : `parent(someChildWillScroll) > child(scroll)`
 */
export const someChildWillScroll = {
  overflow: 'hidden'
}
