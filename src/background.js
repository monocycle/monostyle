import { coalesce } from './strings.js'





export function background() {

  let output = ''

  for (let i = 0; i < arguments.length; i++) {

    const background_1 = arguments[i]
    const backgroundSize = background_1.size
      ? '/' + background_1.size
      : ''

    const backgroundParts = [
      coalesce(background_1.image),
      coalesce(background_1.position) + backgroundSize,
      coalesce(background_1.repeat),
      coalesce(background_1.origin),
      coalesce(background_1.clip),
      coalesce(background_1.attachment),
      coalesce(background_1.color),
    ]

    const backgroundString = backgroundParts.filter(Boolean).join(' ')

    output += (output.length && backgroundString ? ', ' : '') + backgroundString
  }

  return output
}
