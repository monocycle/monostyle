export function isDefined(s) {
  return s !== null && s !== undefined
}

export function isNotEmpty(s) {
  return s !== ''
}
