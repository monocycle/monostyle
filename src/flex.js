

/** If you have more than one child prefer horizontal,vertical */
export const flexRoot = {
  display: [
    '-ms-flexbox',
    '-webkit-flex',
    'flex',
  ]
}

/**
 * A general grouping component that has no impact on the parent flexbox properties e.g.
 * <vertical>
 *    <pass>
 *       <content/>
 *    </pass>
 * </vertical>
 */
export const pass = {
  display: 'inherit',
  '-ms-flex-direction': 'inherit',
  '-webkit-flex-direction': 'inherit',
  flexDirection: 'inherit',
  '-ms-flex-positive': 1,
  '-webkit-flex-grow': 1,
  flexGrow: 1
}

export const inlineRoot = {
  display: [
    '-ms-inline-flexbox',
    '-webkit-inline-flex',
    'inline-flex'
  ]
}

export const horizontal = {
  ...flexRoot,
  '-ms-flex-direction': 'row',
  '-webkit-flex-direction': 'row',
  flexDirection: 'row',
  $debugName: 'row'
}

export const vertical = {
  ...flexRoot,
  '-ms-flex-direction': 'column',
  '-webkit-flex-direction': 'column',
  flexDirection: 'column',
  $debugName: 'col'
}

export const wrap = {
  '-ms-flex-wrap': 'wrap',
  '-webkit-flex-wrap': 'wrap',
  flexWrap: 'wrap'
}

/**
 * If you want items to be sized automatically by their children use this
 * This is because of a bug in various flexbox implementations: http://philipwalton.com/articles/normalizing-cross-browser-flexbox-bugs/
 * Specifically bug 1 : https://github.com/philipwalton/flexbugs#1-minimum-content-sizing-of-flex-items-not-honored
 */
export const content = {
  '-ms-flex-negative': 0,
  '-webkit-flex-shrink': 0,
  flexShrink: 0,
  flexBasis: 'auto'
}

export const flex = {
  '-ms-flex': 1,
  '-webkit-flex': 1,
  flex: 1,
  $debugName: 'flex',
}

export const flex1 = flex

export const flex2 = {
  '-ms-flex': 2,
  '-webkit-flex': 2,
  flex: 2,
  $debugName: 'flex2',
}

export const flex3 = {
  '-ms-flex': 3,
  '-webkit-flex': 3,
  flex: 3,
  $debugName: 'flex3',
}

export const flex4 = {
  '-ms-flex': 4,
  '-webkit-flex': 4,
  flex: 4,
  $debugName: 'flex4',
}

export const flex5 = {
  '-ms-flex': 5,
  '-webkit-flex': 5,
  flex: 5,
  $debugName: 'flex5',
}

export const flex6 = {
  '-ms-flex': 6,
  '-webkit-flex': 6,
  flex: 6,
  $debugName: 'flex6',
}

export const flex7 = {
  '-ms-flex': 7,
  '-webkit-flex': 7,
  flex: 7,
  $debugName: 'flex7',
}

export const flex8 = {
  '-ms-flex': 8,
  '-webkit-flex': 8,
  flex: 8,
  $debugName: 'flex8',
}

export const flex9 = {
  '-ms-flex': 9,
  '-webkit-flex': 9,
  flex: 9,
  $debugName: 'flex9',
}

export const flex10 = {
  '-ms-flex': 10,
  '-webkit-flex': 10,
  flex: 10,
  $debugName: 'flex10',
}

export const flex11 = {
  '-ms-flex': 11,
  '-webkit-flex': 11,
  flex: 11,
  $debugName: 'flex11',
}

export const flex12 = {
  '-ms-flex': 12,
  '-webkit-flex': 12,
  flex: 12,
  $debugName: 'flex12',
}

// Alignment in cross axis 
export const start = {
  '-ms-flex-align': 'start',
  '-webkit-align-items': 'flex-start',
  alignItems: 'flex-start'
}

export const center = {
  '-ms-flex-align': 'center',
  '-webkit-align-items': 'center',
  alignItems: 'center',
  $debugName: 'center'
}

export const end = {
  '-ms-flex-align': 'end',
  '-webkit-align-items': 'flex-end',
  alignItems: 'flex-end',
  $debugName: 'end'
}

// Alignment in main axis 
export const startJustified = {
  '-ms-flex-pack': 'start',
  '-webkit-justify-content': 'flex-start',
  justifyContent: 'flex-start',
  $debugName: 'startJustified'
}

export const centerJustified = {
  '-ms-flex-pack': 'center',
  '-webkit-justify-content': 'center',
  justifyContent: 'center',
  $debugName: 'centerJustified'
}

export const endJustified = {
  '-ms-flex-pack': 'end',
  '-webkit-justify-content': 'flex-end',
  justifyContent: 'flex-end',
  $debugName: 'endJustified'
}

export const aroundJustified = {
  '-ms-flex-pack': 'distribute',
  '-webkit-justify-content': 'space-around',
  justifyContent: 'space-around',
  $debugName: 'aroundJustified'
}

export const betweenJustified = {
  '-ms-flex-pack': 'justify',
  '-webkit-justify-content': 'space-between',
  justifyContent: 'space-between',
  $debugName: 'betweenJustified'
}

// Alignment in both axes
export const centerCenter = {
  ...centerJustified,
  ...center,
  $debugName: 'centerCenter'
}

// Self alignment
export const selfStart = {
  '-ms-flex-item-align': 'start',
  '-webkit-align-self': 'flex-start',
  alignSelf: 'flex-start'
}

export const selfCenter = {
  '-ms-flex-item-align': 'center',
  '-webkit-align-self': 'center',
  alignSelf: 'center'
}

export const selfEnd = {
  '-ms-flex-item-align': 'end',
  '-webkit-align-self': 'flex-end',
  alignSelf: 'flex-end'
}

export const selfStretch = {
  '-ms-flex-item-align': 'stretch',
  '-webkit-align-self': 'stretch',
  alignSelf: 'stretch'
}
