#!/usr/bin/env node

/** This script checks whether each package should be released with
 * a new version according to ComVer https://github.com/staltz/comver.
 * It has two modes: REPORT and ORACLE.
 *
 * It runs in REPORT mode if no additional command line argument was given.
 * For instance, `node check-release.js`. It will display a human readable
 * report on which packages should have new releases.
 *
 * It runs in ORACLE mode is an argument was provided, e.g.,
 * `node check-release.js xstream-run`,
 * it will exit with a status code answering whether the `xstream-run`
 * package should be released with a new version.
 * 0 means no new release is necessary
 * 2 means it should have a new minor version _.x release
 * 3 means it should have a new major version x._ release
 */
import conventionalChangelog from 'conventional-changelog'



var status = {}

let increment = 0 // 0 = nothing, 1 = patch, 2 = minor, 3 = major
let commits = []

const isCommitBreakingChange = commit => (
  typeof commit.footer === 'string' &&
  commit.footer.indexOf('BREAKING CHANGE') !== -1
)


const Report = ({ increment, commits } = {}) => {
  let report = []
  const append = (...args) => {
    report = [
      ...report,
      ...args,
      '\n'
    ]
  }

  if (increment > 0) {

    if (increment !== 2 && increment !== 3)
      throw new Error('This is a ComVer-friendly (https://github.com/staltz/comver) repository ' + increment)

    const index = increment - 2

    append(
      `\n"monostyle" needs a new ${['MINOR', 'MAJOR'][index]} version released because:\n`
    )

    commits.forEach(commit => {

      append('  . ' + commit.header)

      if (isCommitBreakingChange(commit))
        append('    BREAKING CHANGE')

    })

    append('')
  } else {

    append('Nothing to release.')
  }

  return report
}

conventionalChangelog(
  {
    preset: 'angular',
    append: true,
    transform: (commit, cb) => {

      var toPush = null

      if (commit.type === 'fix' || commit.type === 'feat' || commit.type === 'refactor') {
        increment = Math.max(increment, 2)
        toPush = commit
      }

      if (isCommitBreakingChange(commit)) {
        increment = Math.max(increment, 3)
        toPush = commit
      }

      if (toPush)
        commits = [
          ...commits,
          commit
        ]

      if (commit.type === 'release') {
        increment = 0
        commits = []
      }

      cb()
    },
  },
  {},
  { from: '72ca4c4f83f2d6da922394687ba2030a324012a1', reverse: true }
)
  .on('end', () => {

    // ORACLE mode
    var argPackage = process.argv[2]

    if (argPackage)
      return process.exit(increment)

    // REPORT mode
    console.log(...Report({ increment, commits }))

  })
  .resume()