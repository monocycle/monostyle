#!/usr/bin/env bash
exitstatus=0


# echo "$(cat package.json | $(pnpm bin)/jase version)"

# Check if package needs a release
.scripts/checkRelease.js a || needsRelease=$?


# Bump package version
if [ $needsRelease = "2" ]; then
  .scripts/bumpPackageVersion.js --minor || exitstatus=$?;
  kind="MINOR"
elif [ $needsRelease = "3" ]; then
  .scripts/bumpPackageVersion.js --major || exitstatus=$?;
  kind="MAJOR"
else
  echo "Nothing to release"
  exit 0
fi

if [[ $exitstatus != 0 ]]; then

  echo "$packageName failed with code $exitstatus"
  # git stash apply -q
  exit $exitstatus;
fi



# # Push release
git add "package.json"
git commit --no-verify -m "release(package): $(cat package.json | $(pnpm bin)/jase version)"
git push origin master


# Publish release
pnpm publish


echo "✓ Released new $kind for \"monostyle\""