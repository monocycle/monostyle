#!/bin/sh
":" //# http://sambal.org/?p=1014 ; exec /usr/bin/env node --experimental-json-modules "$0" "$@"
import { readFile, writeFile } from 'node:fs/promises'
import Path from 'node:path'

const bumpType = process.argv[2]

const packagePath = Path.join(process.cwd(), 'package.json')


const loadFile = async (path) => readFile(
  path,
  {
    encoding: 'utf8'
  }
)

const loadJsonFile = async path => JSON.parse(
  await loadFile(path)
)

const packageJSON = await loadJsonFile(packagePath)//.then(({ default: packageJSON }) => {

const version = packageJSON.version

let [major, minor, patch, ...others] = version
  .split('.')
  .map(s => parseInt(s, 10))

if (bumpType == '--major') {
  major += 1
  minor = 0
  patch = 0
} else if (bumpType == '--minor') {
  minor += 1
  patch = 0
} else if (bumpType == '--patch') {
  patch += 1
} else {
  console.log(
    'Please specify --major, --minor or --patch as the second argument'
  )
  process.exit(1)
}

packageJSON.version = [major, minor, patch, ...others].join('.')

writeFile(
  packagePath,
  JSON.stringify(packageJSON, null, 2) + '\n'
)
